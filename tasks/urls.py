from django.urls import path
from tasks.views import (
    TaskCreateCreateView,
    TaskListListView,
    TaskUpdateUpdateView,
)

urlpatterns = [
    path("create/", TaskCreateCreateView.as_view(), name="create_task"),
    path("mine/", TaskListListView.as_view(), name="show_my_tasks"),
    path(
        "<int:pk>/complete/",
        TaskUpdateUpdateView.as_view(),
        name="complete_task",
    ),
]
