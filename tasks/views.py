from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks.models import Task
from django.urls import reverse_lazy
from django.views.generic.list import ListView


class TaskCreateCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = [
        "name",
        "start_date",
        "due_date",
        "is_complete",
        "project",
        "assignee",
    ]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project.id])


class TaskListListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateUpdateView(UpdateView):
    model = Task
    template_name = "tasks/update.html"
    fields = ["is_complete"]
    success_url = reverse_lazy("show_my_tasks")

    # def get_success_url(self):
    #     return reverse_lazy("show_my_tasks", args=[self.object.id])
