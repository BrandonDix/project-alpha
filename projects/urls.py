from django.urls import path
from projects.views import (
    ProjectListListView,
    ProjectDetailDetailView,
    ProjectCreateCreateView,
)

urlpatterns = [
    path("", ProjectListListView.as_view(), name="list_projects"),
    path(
        "<int:pk>/detail/",
        ProjectDetailDetailView.as_view(),
        name="show_project",
    ),
    path("create/", ProjectCreateCreateView.as_view(), name="create_project"),
]
