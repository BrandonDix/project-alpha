from django.db import models
from django.conf import settings

usermodel = settings.AUTH_USER_MODEL


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(
        "auth.User", related_name="projects", blank=True
    )

    def __str__(self):
        return self.name
